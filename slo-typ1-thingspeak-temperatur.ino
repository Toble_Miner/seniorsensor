// Temperatursensor mit ESP8266 Mikrocontroller siehe eBook https://sloiot.pressbooks.com 
// programm erstellt von sLO unter Verwendung anderer programme siehe eBook und unter Verwendung
//https://github.com/tzapu/WiFiManager
#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino


#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
  #include "SPI.h"

#include <OneWire.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>  
#include "ThingSpeak.h"
 //Temperatursensordaten///////////////////////////////////////////////////////////////////////////////////////////////////////
//Das Originalprogramm ist unter  http://internetofthinking.blogspot.de/2016/02/upload-ds18b20-temperature-sensor-data.html beschrieben und wurde hier nur angepasst
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#define myPeriodic 15 //in sec | Thingspeak pub is 15sec
#define ONE_WIRE_BUS 2  // DS18B20--- Datenkabel wird  auf  D4 am  physikalischen Board gelegt
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
// Achtung hier Eingabe
const char* MY_SSID = "Ihre WLAN-SSID"; // Eingabe Ihres WLAN-SSID
const char* MY_PWD = "Ihr WLAN-Passwort";// Eingabe Ihres WLAN-Passworts 

 //Temperatursensordate n///////////////////////////////////////////////////////////
  int status = WL_IDLE_STATUS;
  WiFiClient  client;
  // On ESP8266:  0 - 1023 maps to 0 - 1 volts
  #define VOLTAGE_MAX 3.3
  #define VOLTAGE_MAXCOUNTS 1023.0
  #define myPeriodic 15 //in sec | Thingspeak pub is 15sec

String WriteAPIKey1 ="999999999999";// Eingabe des WriteAPI-Key hier geändert

String feldnummer="9";// Eingabe der Feldnummer
String myChannelNumber="99999";

const char* server = "api.thingspeak.com";
float prevTemp = 0;
int sent = 0;
// Ende der Eingabe
//define your default values here, if there are different values in config.json, they are overwritten.

char apiKey_token[34] = "api Key";
char FeldID_token[34] = "";
char Feldnummer_token[34] = "";



char ssid[] = "";
char pass[] = "";
char apiKey[21] = "";
char FeldID[30] = "";
char Feldnummer[30] = "";

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}



void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);// geändert von 9600
  Serial.println();


  //read configuration from FS json
  Serial.println("mounting FS...line 84");

retry:
 if (SPIFFS.begin()) {
  Serial.println("mounted file system line 87");
    if (SPIFFS.exists("/config.json")) {
     //file exists, reading and loading
     Serial.println("reading config file line 90");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
       std::unique_ptr<char[]> buf(new char[size]);
       Serial.println("line 97");

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json moin");
          strcpy(apiKey, json.get<String>("apiKey").c_str());
          strcpy(FeldID, json.get<String>("FeldID_token").c_str());
          Serial.println("line109");//test
          Serial.println(FeldID);//test
        } else {
          Serial.println("failed to load json config line 112");
        }
      }
    }
    
  } else {
    Serial.println("failed to mount FS, formatting...");
    SPIFFS.format();
    goto retry;
  }
  //end read

Serial.println("line139");

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length

  WiFiManagerParameter customApiKey("apiKEY", "API Key", apiKey, 50);
  WiFiManagerParameter custom_FeldID_token("FeldID", "FELDNUMMER", FeldID_token, 10);
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
Serial.println(" line 133");
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
// wifiManager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  
  //add all your parameters here

  wifiManager.addParameter(&customApiKey);
  wifiManager.addParameter(&custom_FeldID_token);
 
  //reset settings - for testing
//wifiManager.resetSettings();

  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
 
  Serial.println("ssid und pwd line 162");

  Serial.print("Auto connect SSID: ");
  Serial.println(WiFi.SSID());
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
  //  reset and try again, or maybe put it to deep sleep
  //wifi.disconnect() before calling startConfigPortal() 2508

 // ESP.reset();
 ESP.restart();//2608
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :line 176)");
   

  //read updated parameters
//Fehlermeldung bei 

  strcpy(apiKey, customApiKey.getValue());  
 strcpy(FeldID, custom_FeldID_token.getValue());


///save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config line 186");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["apikEY"] = apiKey;
    json["FeldID_token"] = FeldID;


    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    Serial.println();
    Serial.print("Saved ");
    Serial.print(json.printTo(configFile));
    Serial.println(" bytes to config");
    configFile.close();
    //end save
 }  
        Serial.println(" local ip line 220");
        Serial.println(WiFi.localIP());

  
ThingSpeak.begin(client);
  Serial.println(" line 224");
 Serial.println(apiKey);

 feldnummer=FeldID;
  Serial.println(FeldID);
 WriteAPIKey1=apiKey;
  Serial.println( WriteAPIKey1);




}

void loop() {
 float temp;
  //char buffer[10];
  DS18B20.requestTemperatures(); 
  temp = DS18B20.getTempCByIndex(0);
  //String tempC = dtostrf(temp, 4, 1, buffer);//handled in sendTemp()
  Serial.print(String(sent)+" Temperature: ");
  Serial.println(temp);
  feldnummer=FeldID;
   WriteAPIKey1=apiKey; 
    Serial.println(" line 231 apiKey und FeldID"); 
   Serial.println(apiKey);   
      Serial.println(FeldID);
  // Write to ThingSpeak. There are up to 8 fields in a channel, allowing you to store up to 8 different
  // pieces of information in a channel.  Here, we write to field 1.

 
  delay(20000); // ThingSpeak nimmt updates nur alle 15 sec an.
//}
   
 
   WiFiClient client;

   if (client.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected  line 245");
  WriteAPIKey1 =apiKey;
   Serial.println(feldnummer);
   Serial.println(FeldID);
   Serial.println(WriteAPIKey1);
   

    String postStr = apiKey;

    postStr += "&field";
    postStr += String(feldnummer);
    postStr += "=";
    postStr += String(temp);
    postStr += "\r\n\r\n";

  
  client.print("POST /update HTTP/1.1\n");
  client.print("Host: api.thingspeak.com\n");

  client.print("Connection: close\n");
  client.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey1 + "\n");
 //  client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
 client.print("Content-Type: application/x-www-form-urlencoded\n");
   client.print("Content-Length: ");
   client.print(postStr.length());
   client.print("\n\n");
   client.print(postStr);
   delay(1000);
   
   }//end if
   sent++;
    
 client.stop();
}//end send
